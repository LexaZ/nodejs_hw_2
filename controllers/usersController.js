const bcrypt = require('bcrypt');
const {User} = require('../models/userModel');

const showProfile = (req, res) => {
  res.status(200).json({user: req.user});
};

const changePassword = async (req, res) => {
  const {oldPassword, newPassword} = req.body;
  const user = await User.findOne({username: req.user.username});
  if (!(await bcrypt.compare(oldPassword, user.password))) {
    res.status(400).json({message: 'Wrong password'});
  }
  await User.findByIdAndUpdate(
      user._id,
      {$set: {password: await bcrypt.hash(newPassword, 10)}},
  );
  res.json({message: 'Your password was successfully changed!'});
};

const deleteProfile = async (req, res) => {
  await User.findByIdAndDelete(req.user._id);
  res.status(200).json({message: 'Your profile was successfully deleted'});
};

module.exports = {
  showProfile,
  changePassword,
  deleteProfile,
};
