const {Note} = require('../models/noteModel');

const postNote = async (req, res) => {
  const {text} = req.body;
  if (!text) {
    return res.status(400).json({message: `Please specify 'text' parameter`});
  }
  const note = new Note({userId: req.user._id, text});
  await note.save();
  res.status(200).json({message: 'Your note was successfully created!'});
};

const getNotes = async (req, res) => {
  const {skip, limit} = req.query;
  const notes = await Note.find(
      {userId: req.user._id},
      [],
      {
        skip: parseInt(skip),
        limit: parseInt(limit),
      },
  );
  if (notes.length === 0) {
    res.status(200).json({notes: []});
  } else {
    res.status(200).json({notes});
  }
};

const getNote = async (req, res) => {
  const id = req.params.id;
  try {
    const note = await Note.findById(id);
    return res.status(200).json({note});
  } catch {
    return res.status(404).json({
      message: `A note with id '${id}' was not found`,
    });
  }
};

const putNote = async (req, res) => {
  const id = req.params.id;
  const {text} = req.body;
  if (!text) {
    return res.status(400).json({message: `Please specify 'text' parameter`});
  }
  try {
    await Note.findByIdAndUpdate(id, {$set: {text}});
    res.status(200).json({message: 'Your note was successfully modified!'});
  } catch {
    return res.status(404).json({
      message: `A note with id '${id}' was not found`,
    });
  }
};

const patchNote = async (req, res) => {
  const id = req.params.id;
  try {
    const {completed} = await Note.findById(id);
    await Note.findByIdAndUpdate(id, {$set: {completed: !completed}});
    res.status(200).json({
      message: 'Status of your note was successfully modified!',
    });
  } catch {
    return res.status(404).json({
      message: `A note with id '${id}' was not found`,
    });
  }
};

const deleteNote = async (req, res) => {
  try {
    await Note.findByIdAndDelete(req.params.id);
    res.status(200).json({message: 'Your note was successfully deleted!'});
  } catch {
    return res.status(404).json({
      message: `A note with id '${req.params.id}' was not found`,
    });
  }
};

module.exports = {
  postNote,
  getNotes,
  getNote,
  putNote,
  patchNote,
  deleteNote,
};
