const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../config');
const {User} = require('../models/userModel');

const registration = async (req, res) => {
  const {username, password} = req.body;
  const user = new User({
    username,
    password: await bcrypt.hash(password, 10),
  });
  await user.save();
  res.status(200).json({message: 'User was created successfully'});
};

const login = async (req, res) => {
  const {username, password} = req.body;
  const user = await User.findOne({username});
  if (!user) {
    return res.status(400).json({
      message: `No user with username '${username}' found!`,
    });
  }
  if (!(await bcrypt.compare(password, user.password)) ) {
    return res.status(400).json({message: 'Wrong password!'});
  }
  const token = jwt.sign({
    username: user.username, _id: user._id, createdDate: user.createdDate,
  }, JWT_SECRET);
  res.status(200).json({message: 'Success', jwt_token: token});
};

module.exports = {
  registration,
  login,
};
