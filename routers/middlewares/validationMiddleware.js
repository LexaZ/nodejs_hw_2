const Joi = require('joi');

module.exports.registrationValidator = async (req, res, next) => {
  const schema = Joi.object({
    username: Joi.string()
        .alphanum()
        .max(30)
        .required(),

    password: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
  });
  await schema.validateAsync(req.body);
  next();
};

module.exports.changePasswordValidator = async (req, res, next) => {
  const schema = Joi.object({
    oldPassword: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
    newPassword: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
  });
  await schema.validateAsync(req.body);
  next();
};
