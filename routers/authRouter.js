const express = require('express');
const {asyncWrapper} = require('./helpers');
const {registrationValidator} = require('./middlewares/validationMiddleware');
const {registration, login} = require('../controllers/authController');

const authRouter = new express.Router();

authRouter.post('/register',
    asyncWrapper(registrationValidator),
    asyncWrapper(registration));
authRouter.post('/login', asyncWrapper(login));

module.exports = authRouter;
