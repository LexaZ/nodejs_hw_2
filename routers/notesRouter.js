const express = require('express');
const {asyncWrapper} = require('./helpers');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {
  postNote,
  getNotes,
  getNote,
  putNote,
  patchNote,
  deleteNote,
} = require('../controllers/notesController');

const notesRouter = new express.Router();
notesRouter.use(authMiddleware);
notesRouter.post('/', asyncWrapper(postNote));
notesRouter.get('/', asyncWrapper(getNotes));
notesRouter.get('/:id', asyncWrapper(getNote));
notesRouter.put('/:id', asyncWrapper(putNote));
notesRouter.patch('/:id', asyncWrapper(patchNote));
notesRouter.delete('/:id', asyncWrapper(deleteNote));

module.exports = notesRouter;
