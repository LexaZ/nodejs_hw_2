/* eslint-disable require-jsdoc */
const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const app = express();

const authRouter = require('./routers/authRouter');
const usersRouter = require('./routers/usersRouter');
const notesRouter = require('./routers/notesRouter');

require('dotenv').config();
const port = process.env.PORT || 8080;

app.use(express.json());
app.use(morgan('dev'));

app.use('/api/auth', authRouter);
app.use('/api/users', usersRouter);
app.use('/api/notes', notesRouter);

app.use((err, req, res, next) => {
  res.status(500).json({message: err.message});
});

const start = async () => {
  await mongoose.connect('mongodb+srv://test-user:qwerty1234@cluster0.dwrzo.mongodb.net/node_hw_2?retryWrites=true&w=majority', {useNewUrlParser: true, useUnifiedTopology: true});

  app.listen(port, () => {
    console.log(`Server works at port ${port}!`);
  });
};

start();

